package utils

import (
	"cloud.google.com/go/civil"
	"strconv"
)

func StringToInt64(text string) (int64, error) {
	value, err := strconv.ParseInt(text, 10, 64)
	return value, err
}

func StringToCivilDate(fileContents string) (civil.Date, error) {
	return civil.ParseDate(fileContents)
}