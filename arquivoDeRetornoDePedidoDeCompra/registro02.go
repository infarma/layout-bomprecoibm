package arquivoDeRetornoDoPedidoDeCompra

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeRetornoDePedidoDeCompra/posicoesArquivoDeRetornoDePedidoDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 02 - Detalhes do Retorno do Pedido de Compra
type Registro02 struct {
	CodigoDoRegistro             string `json:"CodigoDoRegistro"`
	TipoDeIdentificacaoDoProduto int32  `json:"TipoDeIdentificacaoDoProduto"`
	IdentificacaoDoProduto       string `json:"IdentificacaoDoProduto"`
	QuantidadeAtendida           int32  `json:"QuantidadeAtendida"`
	QuantidadeRecusada           int32  `json:"QuantidadeRecusada"`
	MotivoDaRejeicao             string `json:"MotivoDaRejeicao"`
}

func (r *Registro02) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeRetornoDePedidoDeCompra.PosicoesRegistro02

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDeIdentificacaoDoProduto, "TipoDeIdentificacaoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDoProduto, "IdentificacaoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeAtendida, "QuantidadeAtendida")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeRecusada, "QuantidadeRecusada")
	err = posicaoParaValor.ReturnByType(&r.MotivoDaRejeicao, "MotivoDaRejeicao")

	return err
}
