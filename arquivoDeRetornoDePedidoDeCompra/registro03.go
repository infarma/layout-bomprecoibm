package arquivoDeRetornoDoPedidoDeCompra

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeRetornoDePedidoDeCompra/posicoesArquivoDeRetornoDePedidoDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 03 - Rodapé do Retorno do Pedido de Compra
type Registro03 struct {
	CodigoDoRegistro          string `json:"CodigoDoRegistro"`
	TotalDeItensAtendidos     int32  `json:"TotalDeItensAtendidos"`
	TotalDaQuantidadeAtendida int32  `json:"TotalDaQuantidadeAtendida"`
	TotalDeItensReusados      int32  `json:"TotalDeItensReusados"`
	TotalDaQuantidadeRecusado int32  `json:"TotalDaQuantidadeRecusado"`
}

func (r *Registro03) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeRetornoDePedidoDeCompra.PosicoesRegistro03

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.TotalDeItensAtendidos, "TotalDeItensAtendidos")
	err = posicaoParaValor.ReturnByType(&r.TotalDaQuantidadeAtendida, "TotalDaQuantidadeAtendida")
	err = posicaoParaValor.ReturnByType(&r.TotalDeItensReusados, "TotalDeItensReusados")
	err = posicaoParaValor.ReturnByType(&r.TotalDaQuantidadeRecusado, "TotalDaQuantidadeRecusado")

	return err
}