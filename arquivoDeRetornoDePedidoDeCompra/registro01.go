package arquivoDeRetornoDoPedidoDeCompra

import (
	"cloud.google.com/go/civil"
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeRetornoDePedidoDeCompra/posicoesArquivoDeRetornoDePedidoDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 01 - Cabeçalho do Retorno do Pedido de Compra
type Registro01 struct {
	CodigoDoRegistro                         string     `json:"CodigoDoRegistro"`
	IdentificacaoDoPedidoNoCliente           string     `json:"IdentificacaoDoPedidoNoCliente"`
	CNPJDoFornecedor                         int32      `json:"CNPJDoFornecedor"`
	IdentificacaoInicialDoPedidoNoFornecedor string     `json:"IdentificacaoInicialdoPedidoNoFornecedor"`
	IdentificacaoFinalDoPedidoNoFornecedor   string     `json:"IdentificacaoFinalDoPedidoNoFornecedor"`
	PrevisaoDaDataDeFaturamento              civil.Date `json:"PrevisaoDaDataDeFaturamento"`
	PrevisaoDaDataDeEntregaDaMercadoria      civil.Date `json:"PrevisaoDaDataDeEntregaDaMercadoria"`
	StatusDoProcessamentoDoPedido            int32      `json:"StatusDoProcessamentoDoPedido"`
}

func (r *Registro01) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeRetornoDePedidoDeCompra.PosicoesRegistro01

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDoPedidoNoCliente, "IdentificacaoDoPedidoNoCliente")
	err = posicaoParaValor.ReturnByType(&r.CNPJDoFornecedor, "CNPJDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoInicialDoPedidoNoFornecedor, "IdentificacaoInicialDoPedidoNoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoFinalDoPedidoNoFornecedor, "IdentificacaoFinalDoPedidoNoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.PrevisaoDaDataDeFaturamento, "PrevisaoDaDataDeFaturamento")
	err = posicaoParaValor.ReturnByType(&r.PrevisaoDaDataDeEntregaDaMercadoria, "PrevisaoDaDataDeEntregaDaMercadoria")
	err = posicaoParaValor.ReturnByType(&r.StatusDoProcessamentoDoPedido, "StatusDoProcessamentoDoPedido")

	return err
}
