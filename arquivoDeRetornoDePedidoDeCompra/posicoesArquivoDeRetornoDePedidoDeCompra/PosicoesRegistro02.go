package posicoesArquivoDeRetornoDePedidoDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro02 = map[string]posicoes.Posicao{
	"CodigoDoRegistro":             {0, 1},
	"TipoDeIdentificacacDoProduto": {2, 3},
	"IdentificacaoDoProduto":       {4, 16},
	"QuantidadeAtendida":           {17, 27},
	"QuantidadeRecusada":           {28, 38},
	"MotivoDaRejeicao":             {39, 68},
}
