package posicoesArquivoDeRetornoDePedidoDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro01 = map[string]posicoes.Posicao{
	"CodigoDoRegistro":                         {0, 1},
	"IdentificacaoDoPedidoNoCliente":           {2, 13},
	"CNPJDoFornecedor":                         {12, 27},
	"IdentificacaoInicialDoPedidoNoFornecedor": {28, 39},
	"IdentificacaoFinalDoPedidoNoFornecedor":   {40, 51},
	"PrevisaoDaDataDeFaturamento":              {52, 60},
	"PrevisaoDaDataDeEntregaDaMercadoria":      {61, 69},
	"StatusDoProcessamentoDoPedido":            {70, 71},
}
