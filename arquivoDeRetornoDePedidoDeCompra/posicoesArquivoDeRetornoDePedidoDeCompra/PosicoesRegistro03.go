package posicoesArquivoDeRetornoDePedidoDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro03 = map[string]posicoes.Posicao{
	"CodigoDoRegistro":          {0, 1},
	"TotalDeItensAtendidos":     {2, 7},
	"TotalDaQuantidadeAtendida": {8, 18},
	"TotalDeItensReusados":      {19, 24},
	"TotalDaQuantidadeRecusado": {25, 35},
}
