package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Pagamento - Dados das Condições de Pagamento
type RegistroPagamento struct {
	TipoDeRegistro                          int32  `json:"TipoDeRegistro"`
	TipoDoDocumento                         int32  `json:"TipoDoDocumento"`
	NumeroDoPedido                          string `json:"NumeroDoPedido"`
	EANDoComprador                          int32  `json:"EANDoComprador"`
	QualificadorDoTipoDeCondicaoDePagamento string `json:"QualificadorDoTipoDeCondicaoDePagamento"`
	ReferenciaDePrazoDePagto                string `json:"ReferenciaDePrazoDePagto"`
	RefDeTempoCodificada                    string `json:"RefDeTempoCodificada"`
	TipoDePeriodoCodificado                 string `json:"TipoDePeriodoCodificado"`
	NumeroDePeriodos                        int32  `json:"NumeroDePeriodos"`
	DataVencimentoLiquido                   int32  `json:"DataVencimentoLiquido"`
	PorcentagemDaFatura                     int32  `json:"PorcentagemDaFatura"`
	ValorDaParcela                          int32  `json:"ValorDaParcela"`
	PorcentagemDeDescontoFinanceiro         int32  `json:"PorcentagemDeDescontoFinanceiro"`
	ValorDeImpostos                         int32  `json:"ValorDeImpostos"`
	IdentificacaoCondPagto                  string `json:"IdentificacaoCondPagto"`
	DescricaoCondPagto                      string `json:"DescricaoCondPagto"`
	DataBase                                int32  `json:"DataBase"`
	Juros                                   int32  `json:"Juros"`
}

func (r *RegistroPagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor
	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroPagamento

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.QualificadorDoTipoDeCondicaoDePagamento, "QualificadorDoTipoDeCondicaoDePagamento")
	err = posicaoParaValor.ReturnByType(&r.ReferenciaDePrazoDePagto, "ReferenciaDePrazoDePagto")
	err = posicaoParaValor.ReturnByType(&r.RefDeTempoCodificada, "RefDeTempoCodificada")
	err = posicaoParaValor.ReturnByType(&r.TipoDePeriodoCodificado, "TipoDePeriodoCodificado")
	err = posicaoParaValor.ReturnByType(&r.NumeroDePeriodos, "NumeroDePeriodos")
	err = posicaoParaValor.ReturnByType(&r.DataVencimentoLiquido, "DataVencimentoLiquido")
	err = posicaoParaValor.ReturnByType(&r.PorcentagemDaFatura, "PorcentagemDaFatura")
	err = posicaoParaValor.ReturnByType(&r.ValorDaParcela, "ValorDaParcela")
	err = posicaoParaValor.ReturnByType(&r.PorcentagemDeDescontoFinanceiro, "PorcentagemDeDescontoFinanceiro")
	err = posicaoParaValor.ReturnByType(&r.ValorDeImpostos, "ValorDeImpostos")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoCondPagto, "IdentificacaoCondPagto")
	err = posicaoParaValor.ReturnByType(&r.DescricaoCondPagto, "DescricaoCondPagto")
	err = posicaoParaValor.ReturnByType(&r.DataBase, "DataBase")
	err = posicaoParaValor.ReturnByType(&r.Juros, "Juros")

	return err
}
