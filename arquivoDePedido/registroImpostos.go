package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Impostos - Dados dos Impostos do Item
type RegistroImpostos struct {
	TipoDeRegistro          int32  `json:"TipoDeRegistro"`
	TipoDoDocumento         int32  `json:"TipoDoDocumento"`
	NumeroDoPedido          string `json:"NumeroDoPedido"`
	EANDoComprador          int32  `json:"EANDoComprador"`
	NumeroSequencialDaLinha int32  `json:"NumeroSequencialDaLinha"`
	CodigoDoItem            int32  `json:"CodigoDoItem"`
	AliquotaDeIPI           int32  `json:"AliquotaDeIPI"`
	ValorIPI                int32  `json:"ValorIPI"`
	ValorICMS               int32  `json:"ValorICMS"`
}

func (r *RegistroImpostos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroImpostos

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.NumeroSequencialDaLinha, "NumeroSequencialDaLinha")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoItem, "CodigoDoItem")
	err = posicaoParaValor.ReturnByType(&r.AliquotaDeIPI, "AliquotaDeIPI")
	err = posicaoParaValor.ReturnByType(&r.ValorIPI, "ValorIPI")
	err = posicaoParaValor.ReturnByType(&r.ValorICMS, "ValorICMS")

	return err
}
