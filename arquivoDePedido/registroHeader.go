package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Header - Dados do Cabeçalho do Pedido
type RegistroHeader struct {
	TipoDeRegistro                      int32  `json:"TipoDeRegistro"`
	TipoDoDocumento                     int32  `json:"TipoDoDocumento"`
	NumeroDoPedido                      string `json:"NumeroDoPedido"`
	IndicadorDeSegundaVia               int32  `json:"IndicadorDeSegundaVia"`
	IndicadorDeCancelamento             int32  `json:"IndicadorDeCancelamento"`
	ClasseDoPedido                      string `json:"ClasseDoPedido"`
	DataEmissaoPedido                   int32  `json:"DataEmissaoPedido"`
	HoraEmissao                         int32 `json:"HoraEmissao"`
	DataInicioEntrega                   int32  `json:"DataInicioEntrega"`
	HoraInicioEntrega                   string `json:"HoraInicioEntrega"`
	DataFimEntrega                      int32  `json:"DataFimEntrega"`
	HoraFimEntrega                      string `json:"HoraFimEntrega"`
	DataEntrega                         int32  `json:"DataEntrega"`
	HoraEntrega                         string `json:"HoraEntrega"`
	DataInicioDaPromocao                int32  `json:"DataInicioDaPromocao"`
	DataFimDaPromocao                   int32  `json:"DataFimDaPromocao"`
	DataDeEmbarque                      int32  `json:"DataDeEmbarque"`
	DataLimiteDeEmbarque                int32  `json:"DataLimiteDeEmbarque"`
	NumeroDoContrato                    string `json:"NumeroDoContrato"`
	NumeroTabelaDePreco                 string `json:"NumeroTabelaDePreco"`
	TipoDePedido                        int32  `json:"TipoDePedido"`
	CodigoDoTextoLivre                  string `json:"CodigoDoTextoLivre"`
	TipoDeNegociacao                    string `json:"TipoDeNegociacao"`
	CodigoDaCondicaoAceite              string `json:"CodigoDaCondicaoAceite"`
	EANDoComprador                      int64  `json:"EANDoComprador"`
	CGCDoComprador                      int64  `json:"CGCDoComprador"`
	NomeDoComprador                     string `json:"NomeDoComprador"`
	EANDoFornecedor                     int64  `json:"EANDoFornecedor"`
	CGCDoFornecedor                     int64  `json:"CGCDoFornecedor"`
	NomeDoFornecedor                    string `json:"NomeDoFornecedor"`
	CodigoInternoDoFornecedor           string `json:"CodigoInternoDoFornecedor"`
	EANDoLocalDeEntrega                 int64  `json:"EANDoLocalDeEntrega"`
	CGCDoLocalDeEntrega                 int64  `json:"CGCDoLocalDeEntrega"`
	EANDoLocalFatura                    int64  `json:"EANDoLocalFatura"`
	CGCDoLocalFatura                    int64  `json:"CGCDoLocalFatura"`
	EANDoEmissorDoPedido                int64  `json:"EANDoEmissorDoPedido"`
	EANLocalEmbarque                    int64  `json:"EANLocalEmbarque"`
	IdentificacaoDocaDescarga           string `json:"IdentificacaoDocaDescarga"`
	NumeroDaPromocao                    string `json:"NumeroDaPromocao"`
	IdentificacaoDoDepartamentoDeVendas string `json:"IdentificacaoDoDepartamentoDeVendas"`
	CodigoDaMoedaUtilizada              string `json:"CodigoDaMoedaUtilizada"`
}

func (r *RegistroHeader) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor
	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroHeader

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.IndicadorDeSegundaVia, "IndicadorDeSegundaVia")
	err = posicaoParaValor.ReturnByType(&r.IndicadorDeCancelamento, "IndicadorDeCancelamento")
	err = posicaoParaValor.ReturnByType(&r.ClasseDoPedido, "ClasseDoPedido")
	err = posicaoParaValor.ReturnByType(&r.DataEmissaoPedido, "DataEmissaoPedido")
	err = posicaoParaValor.ReturnByType(&r.HoraEmissao, "HoraEmissao")
	err = posicaoParaValor.ReturnByType(&r.DataInicioEntrega, "DataInicioEntrega")
	err = posicaoParaValor.ReturnByType(&r.HoraInicioEntrega, "HoraInicioEntrega")
	err = posicaoParaValor.ReturnByType(&r.DataFimEntrega, "DataFimEntrega")
	err = posicaoParaValor.ReturnByType(&r.HoraFimEntrega, "HoraFimEntrega")
	err = posicaoParaValor.ReturnByType(&r.DataEntrega, "DataEntrega")
	err = posicaoParaValor.ReturnByType(&r.HoraEntrega, "HoraEntrega")
	err = posicaoParaValor.ReturnByType(&r.DataInicioDaPromocao, "DataInicioDaPromocao")
	err = posicaoParaValor.ReturnByType(&r.DataFimDaPromocao, "DataFimDaPromocao")
	err = posicaoParaValor.ReturnByType(&r.DataDeEmbarque, "DataDeEmbarque")
	err = posicaoParaValor.ReturnByType(&r.DataLimiteDeEmbarque, "DataLimiteDeEmbarque")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoContrato, "NumeroDoContato")
	err = posicaoParaValor.ReturnByType(&r.NumeroTabelaDePreco, "NumeroTabelaDePreco")
	err = posicaoParaValor.ReturnByType(&r.TipoDePedido, "TipoDePedido")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoTextoLivre, "CodigoDoTextoLivre")
	err = posicaoParaValor.ReturnByType(&r.TipoDeNegociacao, "TipoDeNegociacao")
	err = posicaoParaValor.ReturnByType(&r.CodigoDaCondicaoAceite, "CodigoDaCondicaoAceite")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.CGCDoComprador, "CGCDoComprador")
	err = posicaoParaValor.ReturnByType(&r.NomeDoComprador, "NomeDoComprador")
	err = posicaoParaValor.ReturnByType(&r.EANDoFornecedor, "EANDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.CGCDoFornecedor, "CGCDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.NomeDoFornecedor, "NomeDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.CodigoInternoDoFornecedor, "CodigoInternoDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.EANDoLocalDeEntrega, "EANDoLocalDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.CGCDoLocalDeEntrega, "CGCDoLocalDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.EANDoLocalFatura, "EANDoLocalFatura")
	err = posicaoParaValor.ReturnByType(&r.CGCDoLocalFatura, "CGCDoLocalFatura")
	err = posicaoParaValor.ReturnByType(&r.EANDoEmissorDoPedido, "EANDoEmissorDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANLocalEmbarque, "EANLocalEmbarque")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDocaDescarga, "IdentificacaoDocaDescarga")
	err = posicaoParaValor.ReturnByType(&r.NumeroDaPromocao, "NumeroDaPromocao")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDoDepartamentoDeVendas, "IdentificacaoDoDepartamentoDeVendas")
	err = posicaoParaValor.ReturnByType(&r.CodigoDaMoedaUtilizada, "CodigoDaMoedaUtilizada")

	return err
}
