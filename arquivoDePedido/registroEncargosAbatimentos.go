package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Encargos/Abatimentos - Dados dos Encargos e Abatimentos do Item
type RegistroEncargosAbatimentos struct {
	TipoDeRegistro                          int32  `json:"TipoDeRegistro"`
	TipoDoDocumento                         int32  `json:"TipoDoDocumento"`
	NumeroDoPedido                          string `json:"NumeroDoPedido"`
	EANDoComprador                          int32  `json:"EANDoComprador"`
	NumeroSequencialDaLinha                 int32  `json:"NumeroSequencialDaLinha"`
	CodigoDoItem                            int32  `json:"CodigoDoItem"`
	QualificadorDeAbatimentoEncargo         string `json:"QualificadorDeAbatimentoEncargo"`
	TipoDoAcordoDoAbatimentoEncargo         string `json:"TipoDoAcordoDoAbatimentoEncargo"`
	TipoRazaoDoAbatimentoEncargo            string `json:"TipoRazaoDoAbatimentoEncargo"`
	PercentualDeAbatimentoEncargo           int32  `json:"PercentualDeAbatimentoEncargo"`
	ValorDoAbatimentoEncargo                int32  `json:"ValorDoAbatimentoEncargo"`
	QuantidadeIndividualDeAbatimentoEncargo int32  `json:"QuantidadeIndividualDeAbatimentoEncargo"`
	TaxaDeAbatimento                        int32  `json:"TaxaDeAbatimento"`
}

func (r *RegistroEncargosAbatimentos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroEncargosAbatimento

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.NumeroSequencialDaLinha, "NumeroSequencialDaLinha")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoItem, "CodigoDoItem")
	err = posicaoParaValor.ReturnByType(&r.QualificadorDeAbatimentoEncargo, "QualificadorDeAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.TipoDoAcordoDoAbatimentoEncargo, "TipoDoAcordoDoAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.TipoRazaoDoAbatimentoEncargo, "TipoRazaoDoAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.PercentualDeAbatimentoEncargo, "PercentualDeAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.ValorDoAbatimentoEncargo, "ValorDoAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeIndividualDeAbatimentoEncargo, "QuantidadeIndividualDeAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&r.TaxaDeAbatimento, "TaxaDeAbatimento")

	return err
}