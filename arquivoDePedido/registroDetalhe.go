package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Detalhe - Dados do Item
type RegistroDetalhe struct {
	TipoDeRegistro                int32  `json:"TipoDeRegistro"`
	TipoDoDocumento               int32  `json:"TipoDoDocumento"`
	NumeroDoPedido                string `json:"NumeroDoPedido"`
	EANDoComprador                int32  `json:"EANDoComprador"`
	NumeroSequencialDaLinha       int32  `json:"NumeroSequencialDaLinha"`
	CodigoDoItem1                 int32  `json:"CodigoDoItem1"`
	TipoDoCodigoDoItem            string `json:"TipoDoCodigoDoItem"`
	QuantidadePedida              int32  `json:"QuantidadePedida"`
	QuantidadeGratuita            int32  `json:"QuantidadeGratuita"`
	UnidadeDeMedida               string `json:"UnidadeDeMedida"`
	QuantidadeDeUnidadesDeConsumo int32  `json:"QuantidadeDeUnidadesDeConsumo"`
	ValorTotalLiquidoDoItem       int32  `json:"ValorTotalLiquidoDoItem"`
	ValorTotalBrutoDoItem         int32  `json:"ValorTotalBrutoDoItem"`
	PrecoUnitarioBruto            int32  `json:"PrecoUnitarioBruto"`
	PrecoUnitarioLiquido          int32  `json:"PrecoUnitarioLiquido"`
	PrecoUnitarioDeCalculoLiquido int32  `json:"PrecoUnitarioDeCalculoLiquido"`
	PrecoUnitarioDeCalculoBruto   int32  `json:"PrecoUnitarioDeCalculoBruto"`
	NumeroDeEmbalagens            int32  `json:"NumeroDeEmbalagens"`
	NumeroDeEmbalagensInternas    int32  `json:"NumeroDeEmbalagensInternas"`
	NumeroTabelaDePreco           int32  `json:"NumeroTabelaDePreco"`
	DescricaoDoItem               string `json:"DescricaoDoItem"`
	CodigoDeProdutoDoFornecedor   string `json:"CodigoDeProdutoDoFornecedor"`
	CodigoDeProdutoDoComprador    string `json:"CodigoDeProdutoDoFornecedor"`
	CodigoDaCor                   string `json:"CodigoDaCor"`
	CodigoDoTamanho               string `json:"CodigoDoTamanho"`
	DataDeEntrega                 int32  `json:"DataDeEntrega"`
	DataInicioDeEntrega           int32  `json:"DataInicioDeEntrega"`
	DataFimDeEntrega              int32  `json:"DataFimDeEntrega"`
	CodigoComplementarDoItem      int32  `json:"CodigoComplementarDoItem"`
	CodigoProjetoPharmalink       int32  `json:"CodigoProjetoPharmalink"`
	DescontoProjetoPharmalink     int32  `json:"DescontoProjetoPharmalink"`
	CodigoDoItem2                 int32  `json:"CodigoDoItem2"`
	EmbalagemProduto              string `json:"EmbalagemProduto"`
	DescontoNegociado             int32  `json:"DescontoNegociado"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.NumeroSequencialDaLinha, "NumeroSequencialDaLinha")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoItem1, "CodigoDoItem")
	err = posicaoParaValor.ReturnByType(&r.TipoDoCodigoDoItem, "TipoDoCodigoDoItem")
	err = posicaoParaValor.ReturnByType(&r.QuantidadePedida, "QuantidadePedida")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeGratuita, "QuantidadeGratuita")
	err = posicaoParaValor.ReturnByType(&r.UnidadeDeMedida, "UnidadeDeMedida")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeUnidadesDeConsumo, "QuantidadeDeUnidadesDeConsumo")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalLiquidoDoItem, "ValorTotalLiquidoDoItem")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalBrutoDoItem, "ValorTotalBrutoDoItem")
	err = posicaoParaValor.ReturnByType(&r.PrecoUnitarioBruto, "PrecoUnitarioBruto")
	err = posicaoParaValor.ReturnByType(&r.PrecoUnitarioLiquido, "PrecoUnitarioLiquido")
	err = posicaoParaValor.ReturnByType(&r.PrecoUnitarioDeCalculoLiquido, "PrecoUnitarioDeCalculoLiquido")
	err = posicaoParaValor.ReturnByType(&r.PrecoUnitarioDeCalculoBruto, "PrecoUnitarioDeCalculoBruto")
	err = posicaoParaValor.ReturnByType(&r.NumeroDeEmbalagens, "NumeroDeEmbalagens")
	err = posicaoParaValor.ReturnByType(&r.NumeroDeEmbalagensInternas, "NumeroDeEmbalagensInternas")
	err = posicaoParaValor.ReturnByType(&r.NumeroTabelaDePreco, "NumeroTabelaDePreco")
	err = posicaoParaValor.ReturnByType(&r.DescricaoDoItem, "DescricaoDoItem")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeProdutoDoFornecedor, "CodigoDeProdutoDoFornecedor")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeProdutoDoComprador, "CodigoDeProdutoDoComprador")
	err = posicaoParaValor.ReturnByType(&r.CodigoDaCor, "CodigoDaCor")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoTamanho, "CodigoDoTamanho")
	err = posicaoParaValor.ReturnByType(&r.DataDeEntrega, "DataDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.DataInicioDeEntrega, "DataInicioDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.DataFimDeEntrega, "DataFimDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.CodigoComplementarDoItem, "CodigoComplementarDoItem")
	err = posicaoParaValor.ReturnByType(&r.CodigoProjetoPharmalink, "CodigoProjetoPharmalink")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoItem2, "CodigoDoItem2")
	err = posicaoParaValor.ReturnByType(&r.DescontoNegociado, "DescontoNegociado")

	return err
}
