package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Sumário - Dados de Sumário do Pedido
type RegistroSumario struct {
	TipoDeRegistro              int32  `json:"TipoDeRegistro"`
	TipoDoDocumento             int32  `json:"TipoDoDocumento"`
	NumeroDoPedido              string `json:"NumeroDoPedido"`
	EANDoComprador              int32  `json:"EANDoComprador"`
	ValorTotalPedido            int32  `json:"ValorTotalPedido"`
	ValorTotalEncargos          int32  `json:"ValorTotalEncargos"`
	ValorTotalMercadorias       int32  `json:"ValorTotalMercadorias"`
	ValorTotalDescontoComercial int32  `json:"ValorTotalDescontoComercial"`
	ValorDespesasAcessorias     int32  `json:"ValorDespesasAcessorias"`
	ValorEncargosFinanceiros    int32  `json:"ValorEncargosFinanceiros"`
	ValorFrete                  int32  `json:"ValorFrete"`
	ValorTotalDeIPI             int32  `json:"ValorTotalDeIPI"`
	ValorTotalDeBonificacao     int32  `json:"ValorTotalDeBonificacao"`
	NumeroTotalDeLinhasDeItem   int32  `json:"NumeroTotalDeLinhasDeItem"`
}

func (r *RegistroSumario) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroSumario

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalPedido, "ValorTotalPedido")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalEncargos, "ValorTotalEncargos")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalMercadorias, "ValorTotalMercadorias")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalDescontoComercial, "ValorTotalDescontoComercial")
	err = posicaoParaValor.ReturnByType(&r.ValorDespesasAcessorias, "ValorDespesasAcessorias")
	err = posicaoParaValor.ReturnByType(&r.ValorEncargosFinanceiros, "ValorEncargosFinanceiros")
	err = posicaoParaValor.ReturnByType(&r.ValorFrete, "ValorFrete")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalDeIPI, "ValorTotalDeIPI")
	err = posicaoParaValor.ReturnByType(&r.ValorTotalDeBonificacao, "ValorTotalDeBonificacao")
	err = posicaoParaValor.ReturnByType(&r.NumeroTotalDeLinhasDeItem, "NumeroTotalDeLinhasDeItem")

	return err
}