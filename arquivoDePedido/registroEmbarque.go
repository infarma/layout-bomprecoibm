package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Embarque - Dados das Condições de Embarque
type RegistroEmbarque struct {
	TipoDeRegistro             int32  `json:"TipoDeRegistro"`
	TipoDoDocumento            int32  `json:"TipoDoDocumento"`
	NumeroDoPedido             string `json:"NumeroDoPedido"`
	EANDoComprador             int32  `json:"EANDoComprador"`
	TipoCondicaoDeEntrega      string `json:"TipoCondicaoDeEntrega"`
	TipoCodigoDaTransportadora int32  `json:"TipoCodigoDaTransportadora"`
	EANCGCDaTransportadora     int32  `json:"EANCGCDaTransportadora"`
	TipoDoVeiculo              string `json:"TipoDoVeiculo"`
	ModoDeTransporte           string `json:"ModoDeTransporte"`
	NomeDaTransportadora       string `json:"NomeDaTransportadora"`
}

func (r *RegistroEmbarque) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor
	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroEmbarque

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.TipoCondicaoDeEntrega, "TipoCondicaoDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.TipoCodigoDaTransportadora, "TipoCodigoDaTransportadora")
	err = posicaoParaValor.ReturnByType(&r.EANCGCDaTransportadora, "EANCGCDaTransportadora")
	err = posicaoParaValor.ReturnByType(&r.TipoDoVeiculo, "TipoDoVeiculo")
	err = posicaoParaValor.ReturnByType(&r.ModoDeTransporte, "ModoDeTransporte")
	err = posicaoParaValor.ReturnByType(&r.NomeDaTransportadora, "NomeDaTransportadora")

	return err
}
