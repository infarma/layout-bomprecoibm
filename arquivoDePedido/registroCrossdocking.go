package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido/posicoesArquivoDePedido"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro Crossdocking - Dados da Grade de Distribuição
type RegistroCrossdocking struct {
	TipoDeRegistro               int32  `json:"TipoDeRegistro"`
	TipoDoDocumento              int32  `json:"TipoDoDocumento"`
	NumeroDoPedido               string `json:"NumeroDoPedido"`
	EANDoComprador               int32  `json:"EANDoComprador"`
	NumeroSequencialDaLinha      int32  `json:"NumeroSequencialDaLinha"`
	CodigoDoItem                 int32  `json:"CodigoDoItem"`
	NrSequencialParcela          int32  `json:"NrSequencialParcela"`
	QuantidadeDaLoja             int32  `json:"QuantidadeDaLoja"`
	EANOuDunsDoLocalDeEntrega    int32  `json:"EANOuDunsDoLocalDeEntrega"`
	TipoDoCodigoDoLocalDeEntrega string `json:"TipoDoCodigoDoLocalDeEntrega"`
}

func (r *RegistroCrossdocking) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDePedido.PosicoesRegistroCrossdocking

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDoDocumento, "TipoDoDocumento")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	err = posicaoParaValor.ReturnByType(&r.EANDoComprador, "EANDoComprador")
	err = posicaoParaValor.ReturnByType(&r.NumeroSequencialDaLinha, "NumeroSequencialDaLinha")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoItem, "CodigoDoItem")
	err = posicaoParaValor.ReturnByType(&r.NrSequencialParcela, "NrSequencialParcela")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDaLoja, "QuantidadeDaLoja")
	err = posicaoParaValor.ReturnByType(&r.EANOuDunsDoLocalDeEntrega, "EANOuDunsDoLocalDeEntrega")
	err = posicaoParaValor.ReturnByType(&r.TipoDoCodigoDoLocalDeEntrega, "TipoDoCodigoDoLocalDeEntrega")

	return err
}