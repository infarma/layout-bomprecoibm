package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroEmbarque = map[string]posicoes.Posicao{
	"TipoDeRegistro":             {0, 1},
	"TipoDoDocumento":            {2, 3},
	"NumeroDoPedido":             {4, 22},
	"EANDoComprador":             {23, 35},
	"TipoCondicaoDeEntrega":      {36, 38},
	"TipoCodigoDaTransportadora": {39, 41},
	"EANCGCDaTransportadora":     {42, 56},
	"TipoDoVeiculo":              {57, 60},
	"ModoDeTransporte":           {61, 63},
	"NomeDaTransportadora":       {64, 98},
}
