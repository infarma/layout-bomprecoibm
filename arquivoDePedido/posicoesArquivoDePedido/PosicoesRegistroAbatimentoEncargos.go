package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroAbatimentoEncargos = map[string]posicoes.Posicao{
	"TipoDeRegistro":                          {0, 1},
	"TipoDoDocumento":                         {2, 3},
	"NumeroDoPedido":                          {4, 22},
	"EANDoComprador":                          {23, 35},
	"QualificadorDeAbatimentoEncargo":         {36, 38},
	"TipoDoAcordoDoAbatimentoEncargo":         {39, 41},
	"TipoRazaoDoAbatimentoEncargo":            {42, 44},
	"PercentualDeAbatimentoEncargo":           {45, 49},
	"ValorDoAbatimentoEncargo":                {50, 66},
	"QuantidadeIndividualDeAbatimentoEncargo": {67, 83},
	"TaxaDeAbatimento":                        {84, 88},
}
