package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroSumario = map[string]posicoes.Posicao{
	"TipoDeRegistro":                      {0, 1},
	"TipoDoDocumento":                      {2, 3},
	"NumeroDoPedido":                      {4, 22},
	"EANDoComprador":                      {23, 35},
	"ValorTotalPedido":                      {36, 52},
	"ValorTotalEncargos":                      {53, 69},
	"ValorTotalMercadorias":                      {70, 86},
	"ValorTotalDescontoComercial":                      {87, 103},
	"ValorDespesasAcessorias":                      {104, 120},
	"ValorEncargosFinanceiros":                      {121, 137},
	"ValorFrete":                      {138, 154},
	"ValorTotalDeIPI":                      {155, 171},
	"ValorTotalDeBonificacao":                      {172, 188},
	"NumeroTotalDeLinhasDeItem":                      {189, 193},
}