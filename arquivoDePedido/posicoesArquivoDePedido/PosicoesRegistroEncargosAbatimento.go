package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroEncargosAbatimento = map[string]posicoes.Posicao{
	"TipoDeRegistro":                          {0, 1},
	"TipoDoDocumento":                         {2, 3},
	"NumeroDoPedido":                          {4, 22},
	"EANDoComprador":                          {23, 35},
	"NumeroSequencialDaLinha":                 {36, 39},
	"CodigoDoItem":                            {40, 53},
	"QualificadorDeAbatimentoEncargo":         {54, 56},
	"TipoDoAcordoDoAbatimentoEncargo":         {57, 59},
	"TipoRazaoDoAbatimentoEncargo":            {60, 62},
	"PercentualDeAbatimentoEncargo":           {63, 67},
	"ValorDoAbatimentoEncargo":                {68, 84},
	"QuantidadeIndividualDeAbatimentoEncargo": {85, 101},
	"TaxaDeAbatimento":                         {102, 106},
}
