package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroObservacaoItem = map[string]posicoes.Posicao{
	"TipoDeRegistro":          {0, 1},
	"TipoDoDocumento":         {2, 3},
	"NumeroDoPedido":          {4, 22},
	"EANDoComprador":          {23, 35},
	"NumeroSequencialDaLinha": {36, 39},
	"CodigoDoItem":            {40, 53},
	"Observacao1":             {54, 123},
	"Observacao2":             {124, 193},
	"Observacao3":             {194, 263},
	"Observacao4":             {264, 333},
	"Observacao5":             {334, 403},
	"Observacao6":             {404, 473},
	"Observacao7":             {474, 543},
	"Observacao8":             {544, 613},
	"Observacao9":             {614, 683},
	"Observacao10":            {684, 753},
	"Observacao11":            {754, 823},
	"Observacao12":            {824, 893},
	"Observacao13":            {894, 963},
	"Observacao14":            {964, 1033},
	"Observacao15":            {1034, 1103},
}
