package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroCrossdocking = map[string]posicoes.Posicao{
	"TipoDeRegistro":               {0, 1},
	"TipoDoDocumento":              {2, 3},
	"NumeroDoPedido":               {4, 22},
	"EANDoComprador":               {23, 35},
	"NumeroSequencialDaLinha":      {36, 39},
	"CodigoDoItem":                 {40, 53},
	"NrSequencialParcela":          {54, 56},
	"QuantidadeDaLoja":             {57, 68},
	"EANOuDunsDoLocalDeEntrega":    {69, 81},
	"TipoDoCodigoDoLocalDeEntrega": {82, 84},
}
