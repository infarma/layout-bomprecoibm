package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroPagamento = map[string]posicoes.Posicao{
	"TipoDeRegistro":  							{0, 1},
	"TipoDoDocumento": 							{2, 3},
	"NumeroDoPedido":  							{4, 22},
	"EANDoComprador":  							{23, 35},
	"QualificadorDoTipoDeCondicaoDePagamento": 	{36, 38},
	"ReferenciaDePrazoDePagto":                	{39, 41},
	"RefDeTempoCodificada":                    	{42, 44},
	"TipoDePeriodoCodificado":                 	{45, 47},
	"NumeroDePeriodos":                        	{48, 50},
	"DataVencimentoLiquido":                   	{51, 58},
	"PorcentagemDaFatura":                     	{59, 63},
	"ValorDaParcela":                          	{64, 81},
	"PorcentagemDeDescontoFinanceiro":         	{82, 86},
	"ValorDeImpostos":                         	{87, 104},
	"IdentificacaoCondPagto":                  	{105, 106},
	"DescricaoCondPagto":                      	{107, 140},
	"DataBase":                                	{141, 148},
	"Juros":                                   	{149, 153},
}
