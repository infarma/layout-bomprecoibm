package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroObservacaoPedido = map[string]posicoes.Posicao{
	"TipoDeRegistro":  {0, 1},
	"TipoDoDocumento": {1, 2},
	"NumeroDoPedido":  {3, 22},
	"EANDoComprador":  {23, 35},
	"Observacao1":     {36, 105},
	"Observacao2":     {106, 175},
	"Observacao3":     {176, 245},
	"Observacao4":     {246, 315},
	"Observacao5":     {316, 385},
	"Observacao6":     {386, 455},
	"Observacao7":     {456, 525},
	"Observacao8":     {526, 595},
	"Observacao9":     {596, 665},
	"Observacao10":    {666, 735},
	"Observacao11":    {736, 805},
	"Observacao12":    {806, 875},
	"Observacao13":    {876, 945},
	"Observacao14":    {946, 1015},
	"Observacao15":    {1016, 1085},
}
