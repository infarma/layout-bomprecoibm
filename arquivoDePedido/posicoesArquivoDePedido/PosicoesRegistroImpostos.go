package posicoesArquivoDePedido

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistroImpostos = map[string]posicoes.Posicao{
	"TipoDeRegistro":          {0, 1},
	"TipoDoDocumento":         {2, 3},
	"NumeroDoPedido":          {4, 22},
	"EANDoComprador":          {23, 35},
	"NumeroSequencialDaLinha": {36, 39},
	"CodigoDoItem":            {40, 53},
	"AliquotaDeIPI":           {54, 58},
	"ValorIPI":                {59, 75},
	"ValorICMS":               {76, 92},
}
