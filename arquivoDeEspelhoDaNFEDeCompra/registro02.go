package arquivoDeEspelhoDaNFEDeCompra

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeEspelhoDaNFEDeCompra/posicoesArquivoDeEspelhoDaNFEDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 02 - Produtos da Nota Fiscal do Pedido de Compra
type Registro02 struct {
	CodigoDoRegistro              string `json:"CodigoDeRegistro"`
	TipoDeIdentificacaoDoProduto  int32  `json:"TipoDeIdentificacaoDoProduto"`
	IdentificacaoDoProduto        string `json:"IdentificacaoDoProduto"`
	EmbalagemDoProduto            string `json:"EmbalagemDoProduto"`
	QuantidadeDeEmbalagens        int32  `json:"QuantidadeDeEmbalagens"`
	CodigoFiscalDeOperacao        int32  `json:"CodigoFiscalDaOperacao"`
	SituacaoTributaria            string `json:"SituacaoTributaria"`
	PrecoDoProduto                int32  `json:"PrecoDoProduto"`
	PercentualDescontoItem        int32  `json:"PercentualDescontoItem"`
	ValorDescontoDoItem           int32  `json:"ValorDescontoDoItem"`
	PercentualDescontoRepasse     int32  `json:"PercentualDescontoRepasse"`
	ValorDescontoDeRepasse        int32  `json:"ValorDescontoDeRepasse"`
	PercentualDoDescontoComercial int32  `json:"PercentualDoDescontoComercial"`
	ValorDoDescontoComercial      int32  `json:"ValorDoDescontoComercial"`
	ValorDespesaAcessorias        int32  `json:"ValorDespesaAcessorias"`
	ValorDespesaEmbalagem         int32  `json:"ValorDespesaEmbalagem"`
}

func (r *Registro02) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeEspelhoDaNFEDeCompra.PosicoesRegistro02

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.TipoDeIdentificacaoDoProduto, "TipoDeIdentificacaoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDoProduto, "IdentificacaoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.EmbalagemDoProduto, "EmbalagemDoProduto")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeEmbalagens, "QuantidadeDeEmbalagens")
	err = posicaoParaValor.ReturnByType(&r.CodigoFiscalDeOperacao, "CodigoFiscalDeOperacao")
	err = posicaoParaValor.ReturnByType(&r.SituacaoTributaria, "SituacaoTributaria")
	err = posicaoParaValor.ReturnByType(&r.PrecoDoProduto, "PrecoDoProduto")
	err = posicaoParaValor.ReturnByType(&r.PercentualDescontoItem, "PercentualDescontoItem")
	err = posicaoParaValor.ReturnByType(&r.ValorDescontoDoItem, "ValorDescontoDoItem")
	err = posicaoParaValor.ReturnByType(&r.PercentualDescontoRepasse, "PercentualDescontoRepasse")
	err = posicaoParaValor.ReturnByType(&r.ValorDescontoDeRepasse, "ValorDescontoDeRepasse")
	err = posicaoParaValor.ReturnByType(&r.PercentualDoDescontoComercial, "PercentualDoDescontoComercial")
	err = posicaoParaValor.ReturnByType(&r.ValorDoDescontoComercial, "ValorDoDescontoComercial")
	err = posicaoParaValor.ReturnByType(&r.ValorDespesaAcessorias, "ValorDespesaAcessorias")
	err = posicaoParaValor.ReturnByType(&r.ValorDespesaEmbalagem, "ValorDespesaEmbalagem")

	return err
}