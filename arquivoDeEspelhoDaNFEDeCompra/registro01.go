package arquivoDeEspelhoDaNFEDeCompra

import (
	"cloud.google.com/go/civil"
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeEspelhoDaNFEDeCompra/posicoesArquivoDeEspelhoDaNFEDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 01 - Cabeçalho do Espelho da Nota Fiscal do Pedido de Compra
type Registro01 struct {
	CodigoDoRegistro                    string     `json:"CodigoDoRegistro"`
	IdentificacaoDoPedidoNoCliente      string     `json:"IdentificacaoDoPedidoNoCliente"`
	CNPJDoEmissorDaNotaFiscal           string     `json:"CNPJDoEmissorDaNotaFiscal"`
	NumeroDaNotaFiscal                  int32      `json:"NumeroDaNotaFiscal"`
	SerieDaNotaFiscal                   string     `json:"SerieDaNotaFiscal"`
	DataDeEmissaoDaNotaFiscal           civil.Date `json:"DataDeEmissaoDaNotaFiscal"`
	CodigoFiscalDeOperacao              int32      `json:"CodigoFiscalDeOperacao"`
	CodigoDoValorFiscal                 int32      `json:"CodigoDoValorFiscal"`
	ValorBrutoDaNotaFiscal              int32      `json:"ValorBrutoDaNotaFiscal"`
	ValorContabilDaNotaFiscal           int32      `json:"ValorContabilDaNotaFiscal"`
	ValorTributadoDaNotaFiscal          int32      `json:"ValorTributadoDaNotaFiscal"`
	ValorDeOutrasDaNotaFiscal           int32      `json:"ValorDeOutrasDaNotaFiscal"`
	BaseDeICMSRetido                    int32      `json:"BaseDeICMSRetido"`
	ValorDoICMSRetido                   int32      `json:"ValorDoICMSRetido"`
	BaseDeICMSDeEntrada                 int32      `json:"BaseDeICMSDeEntrada"`
	ValorDoICMSDeEntrada                int32      `json:"ValorDoICMSDeEntrada"`
	BaseDoIPI                           int32      `json:"BaseDoIPI"`
	ValorDoIPI                          int32      `json:"ValorDoIPI"`
	CGCDaTransportadora                 string     `json:"CGCDaTransportadora"`
	TipoDoFrete                         string     `json:"TipoDoFrete"`
	ValorDoFrete                        int32      `json:"ValorDoFrete"`
	PercentualDoDescontoComercial       int32      `json:"PercentualDoDescontoComercial"`
	PercentualDoDescontoDeRepasseDeICMS int32      `json:"PercentualDoDescontoDeRepasseDeICMS"`
	TotalDeItens                        int32      `json:"TotalDeItens"`
	TotalDeUnidades                     int32      `json:"TotalDeUnidades"`
	TotalDeNotasParaOPedido             int32      `json:"TotalDeNotasParaOPedido"`
	ValorDeDescontoDeICMSNormal         int32      `json:"ValorDeDescontoDeICMSNormal"`
	ValorDeDescontoPIS                  int32      `json:"ValorDeDescontoPIS"`
	ValorDeDescontoDeCofins             int32      `json:"ValorDeDescontoDeCofins"`
	TipoDeNotaFiscalDeEntrada           int32      `json:"TipoDeNotaFiscalDeEntrada"`
	ChaveDeAcessoDaNotaFiscalEletronica string     `json:"ChaveDeAcessoDaNotaFiscalEletronica"`
}

func (r *Registro01) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeEspelhoDaNFEDeCompra.PosicoesRegistro01

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.IdentificacaoDoPedidoNoCliente, "IdentificacaoDoPedidoNoCliente")
	err = posicaoParaValor.ReturnByType(&r.CNPJDoEmissorDaNotaFiscal, "CNPJDoEmissorDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.NumeroDaNotaFiscal, "NumeroDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.SerieDaNotaFiscal, "SerieDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.DataDeEmissaoDaNotaFiscal, "DataDeEmissaoDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.CodigoFiscalDeOperacao, "CodigoFiscalDeOperacao")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoValorFiscal, "CodigoDoValorFiscal")
	err = posicaoParaValor.ReturnByType(&r.ValorBrutoDaNotaFiscal, "ValorBrutoDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.ValorContabilDaNotaFiscal, "ValorContabilDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.ValorTributadoDaNotaFiscal, "ValorTributadoDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.ValorDeOutrasDaNotaFiscal, "ValorDeOutrasDaNotaFiscal")
	err = posicaoParaValor.ReturnByType(&r.BaseDeICMSRetido, "BaseDeICMSRetido")
	err = posicaoParaValor.ReturnByType(&r.ValorDoICMSRetido, "ValorDoICMSRetido")
	err = posicaoParaValor.ReturnByType(&r.BaseDeICMSDeEntrada, "BaseDeICMSDeEntrada")
	err = posicaoParaValor.ReturnByType(&r.ValorDoICMSDeEntrada, "ValorDoICMSDeEntrada")
	err = posicaoParaValor.ReturnByType(&r.BaseDoIPI, "BaseDoIPI")
	err = posicaoParaValor.ReturnByType(&r.ValorDoIPI, "ValorDoIPI")
	err = posicaoParaValor.ReturnByType(&r.CGCDaTransportadora, "CGCDaTransportadora")
	err = posicaoParaValor.ReturnByType(&r.TipoDoFrete, "TipoDoFrete")
	err = posicaoParaValor.ReturnByType(&r.ValorDoFrete, "ValorDoFrete")
	err = posicaoParaValor.ReturnByType(&r.PercentualDoDescontoComercial, "PercentualDoDescontoComercial")
	err = posicaoParaValor.ReturnByType(&r.PercentualDoDescontoDeRepasseDeICMS, "PercentualDoDescontoDeRepasseDeICMS")
	err = posicaoParaValor.ReturnByType(&r.TotalDeItens, "TotalDeItens")
	err = posicaoParaValor.ReturnByType(&r.TotalDeUnidades, "TotalDeUnidades")
	err = posicaoParaValor.ReturnByType(&r.TotalDeNotasParaOPedido, "TotalDeNotasParaOPedido")
	err = posicaoParaValor.ReturnByType(&r.ValorDeDescontoDeICMSNormal, "ValorDeDescontoDeICMSNormal")
	err = posicaoParaValor.ReturnByType(&r.ValorDeDescontoPIS, "ValorDeDescontoPIS")
	err = posicaoParaValor.ReturnByType(&r.ValorDeDescontoDeCofins, "ValorDeDescontoDeCofins")
	err = posicaoParaValor.ReturnByType(&r.TipoDeNotaFiscalDeEntrada, "TipoDeNotaFiscalDeEntrada")
	err = posicaoParaValor.ReturnByType(&r.ChaveDeAcessoDaNotaFiscalEletronica, "ChaveDeAcessoDaNotaFiscalEletronica")

	return err
}
