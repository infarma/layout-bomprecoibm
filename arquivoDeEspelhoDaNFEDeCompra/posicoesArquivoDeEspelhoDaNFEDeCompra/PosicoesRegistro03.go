package posicoesArquivoDeEspelhoDaNFEDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro03 = map[string]posicoes.Posicao{
	"CodigoDoRegistro":                      {0, 1},
	"LoteDeFabricacao":                      {2, 21},
	"QuantidadeDoProduto":                      {22, 32},
	"DataDeValidade":                      {33, 40},
	"DataDeFabricacao":                      {41, 48},
}