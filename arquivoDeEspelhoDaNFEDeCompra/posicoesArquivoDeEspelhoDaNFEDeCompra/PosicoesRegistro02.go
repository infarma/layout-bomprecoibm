package posicoesArquivoDeEspelhoDaNFEDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro02 = map[string]posicoes.Posicao{
	"CodigoDoRegistro":              {0, 1},
	"TipoDeIdentificacaoDoProduto":  {2, 3},
	"IdentificacaoDoProduto":        {4, 18},
	"EmbalagemDoProduto":            {19, 21},
	"QuantidadeDeEmbalagens":        {22, 33},
	"CodigoFiscalDeOperacao":        {34, 38},
	"SituacaoTributaria":            {39, 41},
	"PrecoDoProduto":                {42, 57},
	"PercentualDescontoItem":        {58, 64},
	"ValorDescontoDoItem":           {65, 80},
	"PercentualDescontoRepasse":     {81, 87},
	"ValorDescontoDeRepasse":        {88, 103},
	"PercentualDoDescontoComercial": {104, 110},
	"ValorDoDescontoComercial":      {111, 126},
	"ValorDespesaAcessorias":        {127, 142},
	"ValorDespesaEmbalagem":         {143, 158},
}
