package posicoesArquivoDeEspelhoDaNFEDeCompra

import "bitbucket.org/infarma/layout-bomprecoibm/posicoes"

var PosicoesRegistro04 = map[string]posicoes.Posicao{
	"CodigoDeRegistro":   {0, 1},
	"NumeroDoTitulo":     {2, 12},
	"LocalDePagamento":   {13, 17},
	"DataDeVencimento":   {18, 26},
	"ValorBruto":         {27, 42},
	"ValorLiquido":       {43, 58},
	"DataDeAntecipacao":  {59, 67},
	"DescontoAntecipado": {68, 72},
	"DescontoLimite":     {73, 77},
}
