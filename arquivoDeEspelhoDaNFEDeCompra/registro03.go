package arquivoDeEspelhoDaNFEDeCompra

import (
	"cloud.google.com/go/civil"
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeEspelhoDaNFEDeCompra/posicoesArquivoDeEspelhoDaNFEDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"
)

//Registro 03 - Lote de Fabricação dos Produtos da Nota Fiscal do Pedido de Compra
type Registro03 struct {
	CodigoDoRegistro    string     `json:"CodigoDoRegistro"`
	LoteDeFabricacao    string     `json:"LoteDeFabricacao"`
	QuantidadeDoProduto int32      `json:"QuantidadeDoProduto"`
	DataDeValidade      civil.Date `json:"DataDeValidade"`
	DataDeFabricacao    civil.Date `json:"DataDeFabricacao"`
}

func (r *Registro03) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeEspelhoDaNFEDeCompra.PosicoesRegistro03

	err = posicaoParaValor.ReturnByType(&r.CodigoDoRegistro, "CodigoDoRegistro")
	err = posicaoParaValor.ReturnByType(&r.LoteDeFabricacao, "LoteDeFabricacao")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDoProduto, "QuantidadeDoProduto")
	err = posicaoParaValor.ReturnByType(&r.DataDeValidade, "DataDeValidade")
	err = posicaoParaValor.ReturnByType(&r.DataDeFabricacao, "DataDeFabricacao")

	return err
}
