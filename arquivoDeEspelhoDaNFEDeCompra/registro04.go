package arquivoDeEspelhoDaNFEDeCompra

import (
	"cloud.google.com/go/civil"
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDeEspelhoDaNFEDeCompra/posicoesArquivoDeEspelhoDaNFEDeCompra"
	"bitbucket.org/infarma/layout-bomprecoibm/posicoes"

)

//Registro 04 - Títulos da Nota Fiscal do Pedido de Compra
type Registro04 struct {
	CodigoDeRegistro   string     `json:"CodigoDeRegistro"`
	NumeroDoTitulo     string     `json:"NumeroDoTitulo"`
	LocalDePagamento   string     `json:"LocalDePagamento"`
	DataDeVencimento   civil.Date `json:"DataDeVencimento"`
	ValorBruto         int32      `json:"ValorBruto"`
	ValorLiquido       int32      `json:"ValorLiquido"`
	DataDeAntecipacao  civil.Date `json:"DataDeAntecipacao"`
	DescontoAntecipado int32      `json:"DescontoAntecipado"`
	DescontoLimite     int32      `json:"DescontoLimite"`
}
func (r *Registro04) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeEspelhoDaNFEDeCompra.PosicoesRegistro04

	err = posicaoParaValor.ReturnByType(&r.CodigoDeRegistro, "CodigoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.NumeroDoTitulo, "NumeroDoTitulo")
	err = posicaoParaValor.ReturnByType(&r.LocalDePagamento, "LocalDePagamento")
	err = posicaoParaValor.ReturnByType(&r.DataDeVencimento, "DataDeVencimento")
	err = posicaoParaValor.ReturnByType(&r.ValorBruto, "ValorBruto")
	err = posicaoParaValor.ReturnByType(&r.ValorLiquido, "ValorLiquido")
	err = posicaoParaValor.ReturnByType(&r.DataDeAntecipacao, "DataDeAntecipacao")
	err = posicaoParaValor.ReturnByType(&r.DescontoAntecipado, "DescontoAntecipado")
	err = posicaoParaValor.ReturnByType(&r.DescontoLimite, "DescontoLimite")

	return err
}