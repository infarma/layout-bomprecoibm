package layout_bomprecoibm

import (
	"bitbucket.org/infarma/layout-bomprecoibm/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}